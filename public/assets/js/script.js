( () => {	

    function scrollValue() {
        let navbar = document.getElementById('menu');
    
        let scroll = window.scrollY;
        if (scroll < 30) {
            navbar.style.cssText = "background-color: none; transition: background-color 0.5s ease";
        } else {
            navbar.style.cssText = "background-color: white; transition: background-color 0.5s ease";
        }
    }
    
    window.addEventListener('scroll', scrollValue);

    
})();


// ( () => {	

//     let navbar; 
//     let scrollNav;

//     let whenReady = () =>{
//          navbar = document.getElementById('menu');
//          scrollNav = window.scrollY;
//     }

//     function scrollValueNav() {
//             if (scrollNav < 30) {
//                     navbar.style.cssText = "background-color: none; transition: background-color 0.5s ease";
//                 } else {
//                         navbar.style.cssText = "background-color: white; transition: background-color 0.5s ease";
//                     }
//                 }

//     document.addEventListener('DOMContentLoaded', whenReady);
//     window.addEventListener('scroll', scrollValueNav);
// })();

